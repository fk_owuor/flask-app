# API Endpoints
  > endpoints that illustrate CRUD operations

## Made with
  > Python
    ~ flask
	
### How to run it
  * clone the repo first
  
```sh
git clone https://fk_owuor@bitbucket.org/fk_owuor/flask-app.git
```
  
  * cd into the folder
  
  * create virtual environment
  
```sh
virtualenv env
```
  
  * activate it;
  
  > for windows;
  
```sh
\Scripts\activate
```
  > for linux;
  
```sh
source\env\bin\activate
```
  
  * to install the required external modules run this command;
  
```sh 
pip freeze > requirements.txt
```
  
  * install the modules using the following command;
  
```sh
pip install -r requirements.txt
```
  
  * to start server, run;
  
```sh
python run.py
```
  
|ENDPOINT| **FUNCTIONALITY** | **METHOD** |
|------| ------| ------|
| **/api/v1/register/** | user register |POST |
| **/api/v1/create_post** | create a post |POST |
| **/api/v1/view_posts** | view posts |GET |
| **/api/v1/del_post/<int:postid>** | delete a post |DELETE |
| **/api/v1/modify_post/<int:postid>** | modify a post |UPDATE |

#ngrok link;
[ngrok link](http://febc8a0a.ngrok.io)

  