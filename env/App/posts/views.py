from flask import Flask, Blueprint, jsonify,request
from App.models import *
from .__init__ import *
from functools import wraps
import datetime
import jwt

posts = Blueprint('posts', __name__)

def require_auth(f):
	@wraps(f)
	def authorization(*args, **kwargs):
		token = request.headers.get('x-access_token')
		if not token:
			return jsonify({'message' : 'Missing Token'}), 403
		try:
			data = jwt.decode(token, 'fifi')
		except:
			return jsonify({'message' : 'Invalid Token'}), 408
		return f(*args, **kwargs)
		
	return authorization

@posts.route('/api/v1/create_post', methods= ['POST'])
@require_auth
def create_post():
    title = request.get_json()['title'].strip()
    post = request.get_json()['post'].strip()
    data = jwt.decode(request.headers.get('x-access_token'), 'fifi')
    username = data['user']
    cur.execute("SELECT username FROM users")
    result = cur.fetchone()
    if result is not None:
        cur.execute("INSERT INTO posts(title, post) VALUES (%s, %s);",(title,post))
        conn.commit()
        return jsonify ({'message':'post succesfully created'})
    else:
        return jsonify({'message':'user unauthorized to create a post'})    

@posts.route('/api/v1/view_posts', methods= ['GET'])
def view_posts():
    cur.execute("SELECT * FROM posts")
    posts = cur.fetchall()
    return jsonify (posts)

@posts.route('/api/v1/del_post/<int:postid>', methods= ['DELETE'])
@require_auth
def del_post(postid):
    data = jwt.decode(request.headers.get('X-access_token'),'fifi')
    username = data['user']
    cur.execute("SELECT * FROM posts WHERE postid = '"+str(postid)+"'")
    user = cur.fetchone()
    if user is not None:
        cur.execute("DELETE FROM posts WHERE postid = '"+str(postid)+"'")
    else:
        return jsonify({'message':'You can\'t delete another user\'s post!'})
    conn.commit()
    return jsonify({'message': 'Your post has been deleted'})
    
@posts.route('/api/v1/modify_post/<int:postid>', methods= ['PUT'])
@require_auth 
def modify_post(postid):
    title=request.get_json()['title'].strip()
    post=request.get_json()['post'].strip()
    data = jwt.decode(request.headers.get('X-access_token'), 'fifi')
    username = data ['user']
    cur.execute("SELECT * FROM posts WHERE username = '"+username+"' AND postid = '"+str(postid)+"'")
    result = cur.fetchone()
    if result is None:
        return jsonify({'message':'You are not authorized to modify this post'})
        cur.execute("UPDATE posts SET title= '"+title+"', post='"+post+"' WHERE postid='"+str(postid)+"'")
        conn.commit()
    return jsonify({'message': 'Your post has been modified'}), 200
