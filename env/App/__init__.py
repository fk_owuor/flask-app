from flask import Flask, Blueprint

app = Flask( __name__ )
app.secret_key = 'fifi'

from .users.views import users
from .posts.views import posts
from .route import main

app.register_blueprint(users, url_prefix='/')
app.register_blueprint(posts, url_prefix='/')
app.register_blueprint(main, url_prefix='/')
