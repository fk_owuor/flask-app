from flask import Flask, Blueprint, jsonify, request
from App.models import *
from functools import wraps
import jwt
import datetime
from .__init__ import *
import re

users = Blueprint('users', __name__)

def require_auth(f):
	@wraps(f)
	def authorization(*args, **kwargs):
		token = request.headers.get('x-access_token')
		if not token:
			return jsonify({'message' : 'Missing Token'}), 403
		try:
			data = jwt.decode(token, 'fifi')
		except:
			return jsonify({'message' : 'Invalid Token'}), 408
		return f(*args, **kwargs)
		
	return authorization

def valid_pwd(password):
    if re.match(r'^(?=\S{6,25}$)(?=.*?\d)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[^A-Za-z\s0-9])',password):
        return jsonify({'message':'True'})
    else:
        return jsonify({'message':'False'})
def valid_mail(email):
    if re.match("(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", email) != None:
        return True
    return False     

@users.route('/api/v1/register', methods= ['POST'])
def register():
    name = request.get_json()['name'].strip()
    username = request.get_json()['username'].strip()
    email = request.get_json()['email'].strip()
    password = request.get_json()['password'].strip()
    if len(name)== 0 or len(username)== 0 or len(email)== 0:
        return jsonify({'message':'Field can\'t be blank!'}), 406
    if len(password) < 6:
        return jsonify({'message':'Password should be 6 characters or more!'}), 406
    else:
        cur.execute("SELECT * FROM users WHERE username = '"+username+"' OR email = '"+email+"'")
        if cur.fetchone() is None:
            if valid_pwd(password):
                if valid_mail(email):
                    cur.execute("INSERT INTO users(name,username,email,password)VALUES(%s, %s, %s, %s);",
                    (name, username, email, password))
                else:
                    return jsonify({'message':'Invalid email format'}), 417
            else:
                return jsonify({'message':'Wrong password format'})
        else:
            return jsonify({'message': 'username or email already taken'}), 409
    conn.commit()
    return jsonify({'message': 'Registration successful'})

@users.route('/api/v1/login', methods= ['POST'])
def login():
    username = request.get_json()['username'].strip()
    password = request.get_json()['password'].strip()
    cur.execute("SELECT COUNT(1) FROM users WHERE username = '"+username+"'")
    if cur.fetchone() is not None:
        cur.execute("SELECT * FROM users WHERE username = '"+username+"'")
        for row in cur.fetchall():
            if password == row[3]:
                token= jwt.encode ({'user':username, 'exp':datetime.datetime.utcnow() + datetime.timedelta(minutes=15)}, 'fifi')
                return jsonify({'message': 'Login successful, token expires in 15 minutes', 
                'x-access_token': token.decode('UTF-8')}), 200
            return jsonify({'message':'wrong password'}), 400
    return jsonify({'message':'Invalid username'})
	